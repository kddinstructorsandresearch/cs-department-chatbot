import os
import sys


def main():
    args = sys.argv[1:]

    f = open(args[0], 'r')
    questions = []
    answers = []
    for i, line in enumerate(f):
        q_a = line.split('|')
        questions.append(q_a[0].strip())
        answers.append(q_a[1].strip())

    f.close()
    f = open('piped_in_questions.txt', 'w')
    # f.write('update\n')
    for question in questions:
        f.write(question + '\n')
    f.write('quit\n')
    f.close()

    os.system('python3 faculty.py < piped_in_questions.txt > piped_answers.txt')

    f = open('piped_answers.txt')
    outputs = [line.split('to exit)')[1].strip() for line in f] 
    f.close()
    ## first line will never make you happy
    # if len(f)
    success = True
    for num, question, answer, output in zip(range(len(outputs)), questions, answers, outputs):
        # print('answer: ', answer)
        # print('output: ', output)
        if answer != output:
            success = False
            print(str(num + 1) + '. Incorrect answer for:\n   ' + question)
            print('Expected:\n   ' + answer)
            print('Received:\n   ' + output)
        # Need to skip a line every time
    if success:
        print('Everything answered correctly!')

    os.system('rm piped_in_questions.txt')
    os.system('rm piped_answers.txt')

if __name__ == '__main__':
  main()
