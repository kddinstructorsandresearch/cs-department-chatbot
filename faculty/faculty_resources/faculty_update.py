import urllib.request
import requests
from bs4 import BeautifulSoup
import os.path
import sys
import re

FACULTY_SITE = 'https://csc.calpoly.edu/faculty/'
FACULTY_SITE_BASE = 'https://csc.calpoly.edu'
FACULTY_AND_STAFF_NAMES_GITHUB = 'https://github.com/jbclements/CSC-department-history/wiki/Faculty-&-Staff-Names'
SCHEDULES = 'http://schedules.calpoly.edu/depts_52-CENG_curr.htm'
EMAIL = '@calpoly.edu'

FACULTY_CACHE_FILE = 'faculty_resources/fac.info'
COURSES_CACHE_FILE = 'faculty_resources/courses.info'

NON_TENURE = 'Non Tenure Track Faculty and Visitors'
DEPT_STAFF = 'Csc Department Staff'
CSL_STAFF = 'CSL Staff'

WOMEN_FILE = 'faculty_resources/labeled_women.txt'

faculty_types = ['professor', 'emeriti', 'lecturer', 'staff']
has_research = ['professor', 'emeriti']

def noneToString(s):
     if s == None:
        s = 'None'
     return s

# Try to find and remove middle initials so we can have a broader match
# Also replace special characters, e.g. zoe wood
def normalize_name(name):
  # simple for now, takes out middle initials or names
  return re.sub("\s[A-Za-z\.\s]+\s", " ", name.lower())

class Course:
    def __init__(self, name, sect, type, days, start, end, location):
        self.name = name
        self.sect = sect
        self.type = type
        self.days = days
        self.start = start
        self.end = end
        self.location = location

    def pr(self):
        print('**************')
        print('Name:', self.name)
        print('Sect:', self.sect)
        print('Type:', self.type)
        if self.days:
           print('Days:', self.days)
        if self.start:
            print('Start:', self.start)
        if self.end:
            print('End:', self.end)
        if self.location:
            print('Location:', self.location)
        print('**************')

    def toString(self):
        D = '|'
        return noneToString(self.name) + D + \
                noneToString(self.sect) + D + \
                noneToString(self.type) + D + \
                noneToString(self.days) + D + \
                noneToString(self.start) + D + \
                noneToString(self.end) + D + \
                noneToString(self.location)

class Faculty:
   # Common base class for all faculty

   def __init__(self, name = None, site = None, email = None, facType = None, position = None, \
           office = None, info = None, years = None, major = None, degree = None, alma_mater = None, \
           phone = None, polyrating = None, gender = None, deptHead = None, personal_site = None, \
           pubs = None, research = None):
      self.name = normalize_name(name)
      self.personal_site = personal_site
      self.site = site
      self.email = email
      self.facType = facType
      self.position = position
      self.office = office
      self.research = research
      self.info = info
      self.years = years
      self.major = major
      self.degree = degree
      self.alma_mater = alma_mater
      self.phone = phone
      self.polyrating = polyrating
      self.gender = gender
      self.deptHead = deptHead
      self.pubs = pubs

   def pr(self):
     print('**************')
     print('Name:', self.name)
     print('Site:', self.site)
     print('Email:', self.email)
     print('Type:', self.facType)
     print('Gender:', self.gender)
     if self.personal_site != None:
        print('Personal site:', self.personal_site)
     if self.position != None:
        print('Position:', self.position)
     if self.office != None:
        print('Office:', self.office)
     if self.research != None:
        print('Research:', self.research)
     if self.info != None:
        print('Info:', self.info)
     if self.years:
         print('Years:', self.years)
     if self.major:
         print('Major:', self.major)
     if self.degree:
         print('Degree:', self.degree)
     if self.alma_mater:
         print('Alma Mater:', self.alma_mater)
     if self.phone:
         print('Phone:', self.phone)
     if self.polyrating:
         print('Polyrating:', self.polyrating)
     if self.deptHead:
         print('Dept Head from:', self.deptHead)
     if self.pubs:
         print('publications:', self.pubs)
     print('**************')
   
   def toString(self):
     D = '|'
     li = 'None'
     if self.research != None:
        li = ''
        for r in self.research:
           li += r + D
        li = li[:-1]
     s = noneToString(self.name)+D+noneToString(self.site)+D+noneToString(self.email)+D+\
        noneToString(self.facType)+D+noneToString(self.position)+D+noneToString(self.office)+D+\
        noneToString(self.info)+D+noneToString(self.years)+D+noneToString(self.major)+D+\
        noneToString(self.degree)+D+noneToString(self.alma_mater)+D+noneToString(self.phone)+D+\
        noneToString(self.polyrating)+D+noneToString(self.gender)+D+noneToString(self.deptHead)+D+\
        noneToString(self.personal_site)+D+noneToString(self.pubs)+D+li
     return s
    
pages = {}

# gets the HTML from a webiste specified
# by the given url. Returns a cached page
# if the same url has already been requested
def getHtml(url):
    # normalize url
    if url[-1] == '/':
        url = url[:-1]
    # cache old requests
    #if url not in pages:
    pages[url] = urllib.request.urlopen(url).read()
    return pages[url]

# Checks to see if the personal version of the professors site exists,
# and outputs the string if it does. e.g. http://users.csc.calpoly.edu/~dekhtyar/
def getPersonalSite(name):
    last = name.split(" ")[-1]
    site = FACULTY_SITE_BASE + '/~' + last
    r = requests.head(site)
    if r.status_code == 200 or r.status_code == 302:
        return site
    return None

# Create a new faculty object by merging properties from the 2 supplied faculty objects
# Prefer values from the first faculty object
# first faculty can't be None, second faculty can be and the method will return the first object
def merge_single_faculty_members(faculty1, faculty2):
    if not faculty2:
        return faculty1

    name = faculty1.name or faculty2.name
    site = faculty1.site or faculty2.site
    email = faculty1.email or faculty2.email
    facType = faculty1.facType or faculty2.facType
    position = faculty1.position or faculty2.position 
    office = faculty1.office or faculty2.office
    info = faculty1.info or faculty2.info
    years = faculty1.years or faculty2.years
    major = faculty1.major or faculty2.major
    degree = faculty1.degree or faculty2.degree
    alma_mater = faculty1.alma_mater or faculty2.alma_mater
    research = faculty1.research or faculty2.research
    phone = faculty1.phone or faculty2.phone
    polyrating = faculty1.polyrating or faculty2.polyrating
    gender = faculty1.gender or faculty2.gender
    deptHead = faculty1.deptHead or faculty2.deptHead
    pubs = faculty1.pubs or faculty2.pubs

    return Faculty(name, site=site, email=email, facType=facType, position=position, \
            office=office, info=info, years=years, major=major, degree=degree, \
            alma_mater=alma_mater, phone=phone, polyrating=polyrating, research=research, \
            gender=gender, deptHead=deptHead, pubs=pubs)

# Merges faculty lists by joining on names and replacing any empty properties 
# on one object with the non-empty property from the other object
def merge_faculty(fac_list1, fac_list2):
    # filter out faculty w/out names, we can't merge them, add them to the result list later
    no_name_fac1 = [fac for fac in fac_list1 if not fac]
    no_name_fac2 = [fac for fac in fac_list2 if not fac]

    fac_dict1 = {fac.name: fac for fac in fac_list1 if fac}
    fac_dict2 = {fac.name: fac for fac in fac_list2 if fac}

    merged = []
    for name, faculty1 in fac_dict1.items():
        faculty2 = fac_dict2.pop(name, None)
        merged.append(merge_single_faculty_members(faculty1, faculty2))

    merged += fac_dict2.values()

    return merged + no_name_fac1 + no_name_fac2

def fetch_schedules():
    soup = BeautifulSoup(getHtml(SCHEDULES),"html.parser")
    cscSection = soup.find('span', text='CENG-Computer Science')
    
    currentRow = cscSection.parent.parent.next_sibling.next_sibling.next_sibling.next_sibling
    
    schedules = {}
    facultyList = []
    
    while not currentRow.find('th', recursive=False):
        currentRow, faculty, courses = parseSchedule(currentRow)
        schedules[faculty.name.lower()] = courses
        facultyList += [faculty]

    return (facultyList, schedules)

def parseSchedule(row):
    td = row.find("td", recursive=False, class_="personName")
    td, name = updateAndReadTag(td, lambda tag: parseName(parseLink(tag)))
    td, alias = updateAndReadTag(td, parseLink)
    td, title = updateAndReadTag(td)
    td, phone = updateAndReadTag(td, parseLink)
    td, office = updateAndReadTag(td)

    faculty = Faculty(name=name, email=alias + EMAIL, phone=phone, office=office)

    courses = []
    td = row.find("td", recursive=False, class_="courseName")
    if td:
        courses.append(parseCourse(td))

    for tr in row.contents[1::2]:
        td = tr.find("td", recursive=False, class_="courseName")
        if td:
            courses.append(parseCourse(td))

    row = nextTag(row)

    return (row, faculty, courses)

def parseCourse(td):
    td, course = updateAndReadTag(td, parseLink)
    td, sect = updateAndReadTag(td)
    td, type = updateAndReadTag(td, parseAbbr)
    td, days = updateAndReadTag(td, parseAbbr)
    td, start = updateAndReadTag(td)
    td, end = updateAndReadTag(td)
    _, location = updateAndReadTag(td, parseLink)

    return Course(course, sect, type, days, start, end, location)

def updateAndReadTag(tag, func = lambda tag: tag.text if tag else b''):
    return (nextTag(tag), func(tag).strip())

def nextTag(tag):
    return tag.next_sibling.next_sibling

def parseLink(td):
    a = td.find("a")
    return a.text if a else td.text

def parseAbbr(td):
    abbr = td.find("abbr")
    return abbr.text if abbr else td.text

def parseName(name):
    names = name.split(',')
    last = names[0].strip()
    first = names[-1].strip()
    return first + ' ' + last

def normalize_property(s):
    return s.strip().strip(',').strip().lower().replace('\n', ' ')

def normal_faculty_parse(li):
    name, _, rest = li.string.strip().partition('(')
    years, _, rest = rest.partition(')')
    major_and_degree_and_alma_mater = rest.lstrip(',').split(',')

    major = len(major_and_degree_and_alma_mater) > 0 and normalize_property(major_and_degree_and_alma_mater[0]) or None
    degree = len(major_and_degree_and_alma_mater) > 1 and normalize_property(major_and_degree_and_alma_mater[1]) or None
    alma_mater = len(major_and_degree_and_alma_mater) > 2 and normalize_property(major_and_degree_and_alma_mater[2]) or None

    return Faculty(normalize_property(name), years=normalize_property(years), \
            major=major, degree=degree, alma_mater=alma_mater)
            

def non_tenure_parse(li):
    name, _, rest = li.string.strip().partition('(')
    if not rest:
        return None
    
    years, _, rest = rest.partition(')')
    if not rest:
        return None

    degree_and_alma_mater = rest.lstrip(',').split(',')

    degree = len(degree_and_alma_mater) > 0 and normalize_property(degree_and_alma_mater[0]) or None
    alma_mater = len(degree_and_alma_mater) > 1 and normalize_property(degree_and_alma_mater[1]) or None

    return Faculty(normalize_property(name), years=normalize_property(years),\
            degree=degree, alma_mater=alma_mater)
            

def staff_parse(li):
    name, _, rest = li.string.strip().partition('(')
    if not rest:
        return None

    years, _, rest = rest.partition(')')
    if not rest:
        return None

    position = rest and normalize_property(rest) or None

    return Faculty(normalize_property(name), facType='staff', position=position, 
            years=normalize_property(years))

# Returns the parse function for transforming li elements on the GH page into Faculty objects
def get_parser(tag):
    if tag.name != 'h2' and tag.name != 'h3':
        return None
    title = tag.contents[-1].string
    if not title:
        return None
    title = title.strip()

    if title == NON_TENURE:
        return non_tenure_parse
    elif title == DEPT_STAFF or title == CSL_STAFF:
        return staff_parse
    return normal_faculty_parse


def fetch_faculty_info_from_github():
    soup = BeautifulSoup(getHtml(FACULTY_AND_STAFF_NAMES_GITHUB), "html.parser")
    divs = soup.find_all('div', class_='markdown-body')

    faculty = []
    for div in divs:
        parser = normal_faculty_parse
        for child in div.children:
            new_parser = get_parser(child)
            if new_parser:
                parser = new_parser
                continue
            if child.name == 'ul':
                # map and filter
                faculty += [fac for fac in [parser(li) for li in child.find_all('li')] if fac]
    return faculty

# gets a professor's research interests and personal info (if available)
# from their personal calpoly site
def getResearchAndInfo(fac_type, href):
    if fac_type in has_research:
        soup = BeautifulSoup(getHtml(FACULTY_SITE_BASE + href), "html.parser")
        divs = soup.find_all('div', class_='facultyBlock')
        for div in divs:
            # span = research interests, p = info
            span = div.find('span')
            research = None
            if span != None:
                research = [w.strip().lower() for w in span.text.split('\n') if len(w.strip()) > 0]
            # more info
            info = None
            p = div.find('p')
            if p != None:
                info = p.text
        return research, info
    else:
        return None, None

def add_genders_dept_heads_site(faculty):
    html = getHtml(FACULTY_AND_STAFF_NAMES_GITHUB)
    soup = BeautifulSoup(html, 'html.parser')
    page_links = soup.findAll('a', {"class": "wiki-page-link"})

    f_women = open(WOMEN_FILE, 'r')

    women_by_last = {last : first for (first, last) in (w.strip().lower().split(' ') for w in f_women)}

    heads = {}
    for page_link in page_links:
        match = re.search(r'>(\d{4}\s[^\s]*)\s(\w+)<', page_link.decode())
        if match:
            tenure = match.group(1).replace(' ', '-')
            last_name = match.group(2)
            heads[last_name.lower()] = tenure

    for fac in faculty:
        last_name = fac.name.split(' ')[-1]
        if last_name in heads:
            fac.deptHead = heads[last_name]

        last_name = fac.name.split(' ')[-1]
        first_name = fac.name.split(' ')[0]
        if last_name in women_by_last:

            w_first_name = women_by_last[last_name]
            # if it's a substring of the first
            if first_name in w_first_name or w_first_name in first_name:
                fac.gender = 'woman'
        else:
            fac.gender = 'man'

        if fac.name:
            fac.personal_site = getPersonalSite(fac.name)

    f_women.close()

# extracts faculty info from a HTML tr object into a Faculty object
def getFacultyInfo(fac_type, tr):
    tds = tr.find_all('td')
    if len(tds) > 0:
        nameTd = tds[0]
        # name, site, email
        a = nameTd.find('a')
        name = a.text.lower().replace('  ', ' ')
        href = a['href']
        email = href[href[:-1].rfind("/") + 1:-1] + EMAIL
        # office
        office = None
        if len(tds[-1].text) > 0 and '@' not in tds[-1].text:
            office = tds[-1].text
        position = None
        for p in tds[1].find_all('p'):
            position = p.text
        research, info = getResearchAndInfo(fac_type, href)
        return Faculty(name, site=FACULTY_SITE_BASE + href, email=email, facType=fac_type, \
            position=position, office=office, info=info, research=research)
    else:
        return None

# gets faculty info from a file
def getCachedFacultyInfo():
    D = '|'
    facs = []
    if not os.path.isfile(FACULTY_CACHE_FILE):
        update()
    for line in open(FACULTY_CACHE_FILE, 'r'):
        ## AHH, this hardcoding kills me
        parts = line.split(D)
        params = parts[:17]
        research = parts[17:]
        if len(research) == 1 and research[0].strip() == 'None':
            research = 'None'
        if type(research) == list:
            for i in range(0, len(research)):
                research[i] = research[i].strip()
                
        params.append(research)
        for i in range(0, len(params)):
            if type(params[i]) == str:
                params[i] = params[i].strip()
            if params[i] == 'None':
                params[i] = None
        facs.append(Faculty(*params))
    return facs

# gets course info from a file
def getCachedCoursesInfo():
    D = '|'
    courses = {}
    if not os.path.isfile(COURSES_CACHE_FILE):
        update()
    for line in open(COURSES_CACHE_FILE, 'r'):
        parts = line.split(D)
        name = parts[0];
        params = parts[1:]
        courses[name] = courses.get(name, []) + [Course(*params)]

    return courses

# gets detailed faculty info from every faculty member
# listed on the CSC faculty page (live data)
def fetchFromFacultySite():
    soup = BeautifulSoup(getHtml(FACULTY_SITE), "html.parser")
    tables = soup.find_all('table', class_='blankTable faculty')

    faculty = []
    for table in tables:
        fac_type = faculty_types[0]
        for ty in faculty_types[1:]:
            if ty in table['summary'].lower():
                fac_type = ty
                break
        for tr in table.find_all('tr'):
            fac = getFacultyInfo(fac_type, tr)
            if fac != None:
                #fac.pr()
                faculty.append(fac)
    return faculty

#NOTE: Dehktyar has 5+ different spellings, may merge later
def fetchPolyratings():
    soup = BeautifulSoup(getHtml("http://polyratings.com/list.phtml"),"html.parser")
    scheduleHTML = soup.findAll('table')[1]
    faculty = []

    for tr in scheduleHTML.find_all('tr'):
        tds = tr.find_all('td')
        dept = tds[1].text.strip()
        if dept == 'CPE' or dept == 'CSC':
            pieces = tds[0].text.strip().split(',')
            name = (pieces[1] + ' ' + pieces[0]).strip().lower()
            rating = tds[3].text.strip()
            faculty.append(Faculty(name, polyrating=rating))
  
    return faculty

def add_publication_info(faculty):
    soup = BeautifulSoup(getHtml("http://digitalcommons.calpoly.edu/fac_authors.html"),"html.parser")
    pubInfo = {}

    for p in soup.findAll('p')[6:-3]:
        pubs = p.text.split(':')[-1][:-1].strip()
        name = p.text.split('(')[0].split(',')
        fullname = (name[-1].strip().lower() + ' ' + name[0].strip().lower()).strip()
        pubInfo[fullname] = pubs

    for fac in faculty:
        if fac.name in pubInfo:
            fac.pubs = pubInfo[fac.name]
            print(fac.name)

def getLiveInfo():
    facultyFromFacultySite = fetchFromFacultySite()
    facultyFromGithub = fetch_faculty_info_from_github()
    facultyFromPolyratings = fetchPolyratings()

    (facultyFromSchedules, courses) = fetch_schedules()

    facultyAndGithubMerged = merge_faculty(facultyFromFacultySite, facultyFromGithub)
    facultyAndPolyratingsMerged = merge_faculty(facultyAndGithubMerged, facultyFromPolyratings)
    facultyAndSchedulesMerged = merge_faculty(facultyAndPolyratingsMerged, facultyFromSchedules)

    add_genders_dept_heads_site(facultyAndSchedulesMerged)
    add_publication_info(facultyAndSchedulesMerged)

    return (facultyAndSchedulesMerged, courses)
    
'''*********************'''
'''*** CALL THIS ONE ***'''
'''*********************'''
# gets detailed faculty info from every faculty member
# listed on the CSC faculty page (live or cached)
def getAllInfo(cached):
    if cached:
        return (getCachedFacultyInfo(), getCachedCoursesInfo())
    return getLiveInfo()

# saves info to a pipe (|) separated file
def update():
    try:
        print('Updating: please wait...')
        (facs, courses) = getAllInfo(False)
        with open(FACULTY_CACHE_FILE, 'w+') as f:
            for fac in facs:
                f.write(fac.toString() + '\n')
        with open(COURSES_CACHE_FILE, 'w+') as f:
            for name, courseList in courses.items():
                for course in courseList:
                    f.write(name + '|' + course.toString() + '\n')
        print('Updated.')
        return True
    except ValueError as e:
        print('Error updating.', e)
        return False
