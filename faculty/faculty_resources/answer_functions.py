from faculty_resources import faculty_update
from faculty_resources import faculty_rep_vars

# put functions you wish to be called when a
# specific question is asked in here. To call
# a function, put "[function]myFunction" as the
# answer to the question in the questions file.

# The function will be called with the query and
# the extracted variables (in order), and will
# need to return a string, which will be displayed
# as the answer.

# You can use "faculty_rep_vars.getInfo()" to get
# all the information we currently have on the
# faculty members to help extract information.

def getNameVars(varList):
    names = []
    for v in varList:
        if v[0] == faculty_rep_vars.NAME_VAR:
            names.append(v[1])
    return names

def getCourseVars(varList):
    courses = []
    for v in varList:
        if v[0] == faculty_rep_vars.COURSE_VAR:
            courses.append(v[1])
    return courses

def getRelevantFaculty(names):
    info =  faculty_rep_vars.getInfo()
    pairs = {}
    for fac in info:
        for name in names:
            rname = faculty_rep_vars.removeNameAdditions(name)
            if rname in fac.name:
                pairs[name] = fac
    return pairs

def getRelevantCourses(names):
    courseMap = faculty_rep_vars.getCoursesByTeacher()
    relevantCourses = {}
    for teacher, courses in courseMap.items():
        for name in names:
            resolvedName = faculty_rep_vars.removeNameAdditions(name)
            if resolvedName in teacher:
                relevantCourses[name] = courses
    return relevantCourses

def getRelevantCoursesByName(courseNames):
    courseMap = faculty_rep_vars.getCoursesByTeacher()
    relevantCourses = {}
    for teacher, courseList in courseMap.items():
        for name in courseNames:
            coursesIntersection = [course for course in courseList if name.strip().lower() == course.name.strip().lower()] 

            relevantCourses[name] = relevantCourses.get(name, []) + [(teacher, course) for course in coursesIntersection]

    return relevantCourses

def getListMessage(names, valName, nameToValue):
    return getListMessageUsingStringFunction(names, lambda val: valName + str(val), nameToValue)

def getListMessageUsingStringFunction(names, valToString, nameToValue, allowFalse = False):
    # building message
    msg = ''
    for ndx in range(0, len(names)):
        name = names[ndx]
        answer = nameToValue.get(name, None) if allowFalse else (nameToValue[name] if name in nameToValue and nameToValue[name] else 'unknown')
        if ndx == 0:
            msg = name.title() + valToString(answer)
            if len(names) == 1:
                msg += '.'
        elif ndx == len(names) - 1:
            msg += ', and ' + name.title() + valToString(answer) + '.'
        else:
            msg += ', ' + name.title() + valToString(answer)
    return msg

def findEmail(query, varList):
    # this will return a message giving the email
    # for every name variable given in the query.
    # get emails
    names = getNameVars(varList)
    emails = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        emails[name] = faculty[name].email
    return getListMessage(names, "'s email is ", emails)

def findPosition(query, varList):
    # this will return a message giving the email
    # for every name variable given in the query.
    # get emails
    names = getNameVars(varList)
    positions = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        positions[name] = faculty[name].facType
    return getListMessage(names, ' is a ', positions)
    
def findResearch(query, varList):
    # this will return a message with the research
    # interests of all the [name] variables
    names = getNameVars(varList)
    fields = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        rs = faculty[name].research
        li = ''
        if rs:
            for ndx in range(0, len(rs)):
                r = rs[ndx]
                if ndx > 0:
                    if len(rs) > 2:
                        li += ', '
                    else:
                        li += ' '
                if ndx == len(rs) - 1:
                    li += 'and ' 
                li += r.title()
            fields[name] = li 
    return getListMessage(names, ' researches ', fields)
    
def findSpecialists(query, varList):
    info =  faculty_rep_vars.getInfo()
    # get all research info
    allFields = {}
    for fac in info:
        if fac.research != None:
            for field in fac.research:
                if field not in allFields:
                    allFields[field] = []
                allFields[field].append(fac.name)
    # TODO handle and/or
    # build msg
    f = None
    for var in varList:
        if var[0] == faculty_rep_vars.FIELD_VAR:
            f = var[1]
    who = allFields[f]
    msg = str(len(who)) + ' professors study ' + f.title() + '. Professors '
    for ndx in range(0, len(who)):
        name = who[ndx]
        if ndx > 0:
            if len(who) > 2:
                msg += ', '
            else:
                msg += ' '
        if ndx == len(who) - 1:
            msg += 'and ' 
        msg += name.split()[1].title()
    msg += ' list it as one of their specialties.'
    return msg

def findPhoneNumber(query, varList):
    names = getNameVars(varList)
    phones = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        phones[name] = faculty[name].phone
    return getListMessage(names, "'s office phone number is ", phones)

def findAlmaMater(query, varList):
    #improve to differentiate undergrad and grad school?
    names = getNameVars(varList)
    faculty = getRelevantFaculty(names)
    msg = ''
    for name in names:
        if faculty[name] == 'None':
            msg += name + ' went to ' + faculty[name].alma_mater + '. '
        else:
            msg += "I'm not sure where " + name + ' went to college. ' 
    return msg

def findSite(query, varList):
    names = getNameVars(varList)
    sites = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        sites[name] = faculty[name].site
    return getListMessage(names, "'s website is ", sites)

def findYearsAtCalPoly(query, varList):
    names = getNameVars(varList)
    years = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        years[name] = faculty[name].years
    return getListMessage(names, " works/worked at Cal Poly: ", years)

def findOffice(query, varList):
    names = getNameVars(varList)
    offices = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        offices[name] = faculty[name].office
    return getListMessage(names, "'s office is in ", offices)

def findPolyrating(query, varList):
    names = getNameVars(varList)
    ratings = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        ratings[name] = faculty[name].polyrating
    return getListMessage(names, "'s polyrating (out of 4) is ", ratings)

def createStringFromContactTuple(email_and_phone):
    email = email_and_phone[0]
    phone = email_and_phone[1]

    if not email and not phone:
        return ' has no known contact info'

    email_msg = 'email is ' + email if email else ''
    phone_msg = 'phone is ' + phone if phone else ''

    return '\'s ' + (email_msg + ' and ' + phone_msg if email and phone else email_msg or phone_msg)

def findContactInfo(query, varList):
    names = getNameVars(varList)
    faculty = getRelevantFaculty(names)
    contactInfo = { name: (faculty[name].email, faculty[name].phone) for name in names}

    return getListMessageUsingStringFunction(names, createStringFromContactTuple, contactInfo)

def createCoursesString(courses):
    return ' teaches the following courses: ' + ', '.join(set([course.name for course in courses]))

def findCoursesForTeacher(query, varList):
    names = getNameVars(varList)
    courseMap = getRelevantCourses(names)

    return getListMessageUsingStringFunction(names, createCoursesString, courseMap)

def createMostFrequentTeacherString(courses):
    sectionCount = {}
    for teacher, _ in courses:
        sectionCount[teacher] = sectionCount.get(teacher, 0) + 1
    sortedCount = sorted(sectionCount, key=sectionCount.get, reverse=True)

#    print(', '.join([teacher + ': ' + str(sectionCount[teacher]) for teacher in sortedCount]))

    mostFrequentTeacher = sortedCount[0]

    return ' is being taught most by ' + mostFrequentTeacher 
    
    
def findTeacherForCourse(query, varList):
    courses = getCourseVars(varList)
    courseMap = getRelevantCoursesByName(courses)

    return getListMessageUsingStringFunction(courses, createMostFrequentTeacherString, courseMap)

def findGender(query, varList):
    # assuming one name in query
    try:
        name = getNameVars(varList)[0]
        faculty = getRelevantFaculty([name])
        return faculty[name].name.title() + ' is a ' + faculty[name].gender + '.'
    except KeyError as e:
        return 'Unable to find gender.'

def getFacStartEnd():
    faculty =  faculty_rep_vars.getInfo()
    output = {}
    for fac in faculty:
        if fac.years:
            lst_years = fac.years.split('-', maxsplit=2)
            if len(lst_years) == 2:
                start = lst_years[0]
                end = lst_years[1]
            elif fac.years[-1] == '-':
                start = fac.years[:-1]
                end = None
            else:
                start = fac.years
                end = fac.years
            output[fac] = (start, end)

    return output    

def findTeachersByYear(query, varList):
    # assumes single year of input
    faculty =  faculty_rep_vars.getInfo()
    year = None
    for tup in varList:
        if tup[0] == '[year]':
            year = int(tup[1])
            break
    if 'before' in query:
        time_frame = 'before'
    elif 'after' in query:
        time_frame = 'after'
    else:
        time_frame = 'in'

    out_fac = []
    faculty_start_end = getFacStartEnd()
    for fac, times in faculty_start_end.items():
        start = times[0]
        end = times[1]
        try: 
            if time_frame == 'before':
                if int(start) < year:
                    out_fac.append(fac.name)
            elif time_frame == 'after':
                if int(start) > year:
                    out_fac.append(fac.name)
            else:
                if int(start) == year:
                    out_fac.append(fac.name)
        except ValueError:
            # print('error', fac.name, start)
            pass

    if len(out_fac) == 0:
        return 'No professors began teaching in ' + str(year) + '.'
    return ', '.join([f.title() for f in out_fac]) + (' started %s %s.' % (time_frame, year))

def checkInDept(query, varList):
    name = getNameVars(varList)[0]
    faculty = getRelevantFaculty([name])
    if len(faculty) > 0:
        return 'Yes'
    return 'No'

def findLastHire(query, varList):
    faculty_start_end = getFacStartEnd()
    recent_year = None
    recent_facs = None
    for fac in faculty_start_end:
        start = faculty_start_end[fac][0]
        try:
            start = int(start)
            if not recent_year:
                recent_year = start
                recent_fac = [fac.name]
            elif start > recent_year:
                recent_facs = [fac.name]
            elif start == recent_year:
                recent_facs.append(fac.name)
        except ValueError:
            # print('error', fac.name, start)
            pass

    if len(recent_facs) > 1: 
        return ', '.join([name.title for name in recent_facs]) + ' all began' + \
        ' in ' + str(recent_year)
    else:
        return recent_facs[0] + ' began in ' + str(start) + '.'


def createCourseCountString(courses):
    numberCourses = len(set([course.name for course in courses]))
    return ' teaches ' + str(numberCourses) + ' courses'

def countCoursesForTeacher(query, varList):
    names = getNameVars(varList)
    courseMap = getRelevantCourses(names)

    return getListMessageUsingStringFunction(names, createCourseCountString, courseMap)

def isTeaching(query, varList):
    names = getNameVars(varList)
    courseMap = getRelevantCourses(names)

    teachingMap = {teacher: (len(courseMap.get(teacher, [])) != 0) for teacher in courseMap}

    return getListMessageUsingStringFunction(names, \
            lambda teaching: ' is ' + ('not ' if not teaching else '') \
            + 'teaching this quarter',\
            teachingMap, allowFalse=True)

def mostFrequentCourseForTeacher(query, varList):
    names = getNameVars(varList)
    courseMap = getRelevantCourses(names)

    frequentCourses = {teacher: mostFrequentCourse(courses) for teacher, courses in courseMap.items() }

    return getListMessageUsingStringFunction(names, lambda course: ' teaches ' + course + ' most often', frequentCourses)

def mostFrequentCourse(courses):
    counts = {}
    for course in courses:
        counts[course.name] = counts.get(course, 0) + 1

    return sorted(counts, key=counts.get, reverse=True)[0] if len(counts) != 0 else 'unknown'

def findPublications(query, varList):
    names = getNameVars(varList)
    pubs = {}
    faculty = getRelevantFaculty(names)
    for name in names:
        pubs[name] = faculty[name].pubs
    return getListMessageUsingStringFunction(names, lambda pub: ' has ' + pub + ' publications in the Cal Poly Digital Commons', pubs)

def getAllCoursesForTeacher(query, varList):
    name = getNameVars(varList)[0]
    courses = getRelevantCourses([name])[name]
    out_name = getRelevantFaculty([name])[name].name.title( )

    return out_name + ' is teaching the following: ' + ', '.join(sorted(set([course.name for course in courses]))) + '.'

def getFacultyPosition(query, varList):
    name = getNameVars(varList)[0]
    fac = getRelevantFaculty([name])[name]
    if fac.position:
        return fac.name.title() + ' is a ' + fac.position + '.'
    else:
        return fac.name.title() + "'s position is not recorded." 

def getPositionCounts(query, varList):
    faculty = faculty_rep_vars.getInfo()
    positions = {}
    for fac in faculty:
        pos = fac.facType or fac.position
        if pos in positions:
            positions[pos] += 1
        else:
            positions[pos] = 1
    return 'The department has the following number of people at each position: ' + \
    ', '.join(sorted(key+' : '+str(value) for key, value in positions.items() if key))

def followUp(query, varList):
    name = getNameVars(varList)[0]
    fac = getRelevantFaculty([name])
    #names = [name for name in names]
    #return "FOLLOWUP:" + ", ".join(names)
    if name in fac:
        return "FOLLOWUP:" + name
    else:
        return "Nothing, I don't know " + name

def findAdvisor(query, varList):
    last_name = input('Please provide your last name.\n')
    if last_name and \
       len(last_name) > 0 and \
       last_name[0].lower() in faculty_rep_vars.faculty_advisors:
        return 'Your recommended faculty advisor is ' + faculty_rep_vars.faculty_advisors[last_name[0].lower()] + '.'
    else:
        return 'Invalid last name provided.'

def checkFacultyAdvisor(query, varList):
    name = getNameVars(varList)[0]
    fac = getRelevantFaculty([name])[name]
    last_name = fac.name.split(' ')[1].title()
    if last_name in faculty_rep_vars.faculty_advisors.values():
        return fac.name.title() + ' is a faculty advisor.'
    else:
        return fac.name.title() + ' is not a faculty advisor.'


def findAllResearch(query, varList):
    # this will return a message with the research
    # interests of all the [name] variables
    fields = {}
    faculty = faculty_rep_vars.getInfo()
    for fac in faculty:
        name = fac.name
        rs = fac.research
        li = ''
        if rs:
            for ndx in range(0, len(rs)):
                r = rs[ndx]
                if ndx > 0:
                    if len(rs) > 2:
                        li += ', '
                    else:
                        li += ' '
                if ndx == len(rs) - 1:
                    li += 'and ' 
                li += r.title()
            fields[name] = li 
    return 'Cal Poly professors have the following research field interests: ' + ', '.join(sorted(fields.values())) + '.'

def checkIfDeptHead(query, varList):
    name = getNameVars(varList)[0]
    fac = getRelevantFaculty([name])[name]
    if fac.deptHead:
        return fac.name.title() + ' was department head from ' + fac.deptHead + '.'
    else:
        return fac.name.title() + ' was never department head.'

def getFacCount(query, varList):
    faculty = faculty_rep_vars.getInfo()
    return "There are " + str(len(faculty)) + " faculty members in the department."

def findActiveFaculty(query, varList):
    faculty = faculty_rep_vars.getInfo()
    active_faculty = []
    for fac in faculty:
        try:
            years = fac.years.split('-')
            if years[1] == '':
                active_faculty.append(fac.name.title())
        except:
            pass
    return 'All active faculty are: ' + ', '.join(sorted(active_faculty))
