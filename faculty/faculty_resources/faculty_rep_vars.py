from faculty_resources import faculty_update
import sys
import re

DEPT_VAR = '[department]'
GENDER_VAR = '[gender]'
LINK_VAR = '[link]'
EMAIL_VAR = '[email]'
NAME_VAR = '[name]'
BUILDING_VAR = '[building]'
ROOM_VAR = '[room]'
FIELD_VAR = '[research field]'
POS_VAR = '[position]'
COURSE_VAR = '[course]'
YEAR_VAR = '[year]'

variables = [
    (DEPT_VAR, ['cal poly csc department', 'cal poly cs department', 'cal poly computer science department', 'csc department', 'cs department', 'computer science department', 'department']),
    (GENDER_VAR, [r'[regex]\bwoman\b', r'[regex]\bman\b', r'[regex]\bfemale\b', r'[regex]\bmale\b']),
    (LINK_VAR, ['[regex]http[a-z0-9\/\:\.]+']),
    (EMAIL_VAR, ['[regex]([\w+\.?]+)\@([(\w+)\.?]+)']),
    (YEAR_VAR, ['[regex](19[3-9][0-9]|200[0-9]|201[0-6])+'])
]

akas = {
    'nlp': 'natural langauge processing',
    'ai' : 'artificial intelligence'
}

faculty_advisors = {
                    'a':'Bellardo',
                    'b':'Bellardo',
                    'c':'Clements',
                    'd':'Clements',
                    'e':'Dekhtyar',
                    'f':'Dekhtyar',
                    'g':'Gharibyan',
                    'h':'Gharibyan',
                    'i':'Haungs',
                    'j':'Haungs',
                    'k':'Kearns',
                    'l':'Keen',
                    'm':'Keen',
                    'n':'Khosmood',
                    'o':'Khosmood',
                    'p':'Kurfess',
                    'q':'Kurfess',
                    'r':'Lupo',
                    's':'Nico',
                    't':'Peterson',
                    'u':'Seng',
                    'v':'Seng',
                    'w':'Staley',
                    'x':'Staley',
                    'y':'Sueda',
                    'z':'Sueda'
                   }

facInfo = None
coursesByTeacher = None
regexTag = '[regex]'

def refresh():
    global facInfo
    global coursesByTeacher
    (facInfo, coursesByTeacher) = faculty_update.getAllInfo(True)
    buildVars()

def removeAdditions(string, adds):
    for add in adds:
        string = string.replace(add, '')
    return string.strip()

def removeNameAdditions(string):
    return removeAdditions(string, ['professor', 'dr.', 'dr', 'doctor'])

def buildVars():
    info = getInfo()
    names = []
    buildings = set()
    rooms = set()
    fields = set()
    positions = set()
    for fac in info:
        # names
        if fac.facType != 'staff':
            names.append('professor ' + fac.name)
            names.append('professor ' + fac.name.split()[-1])
            names.append('dr. ' + fac.name.split()[-1])
            names.append('doctor ' + fac.name.split()[-1])
            names.append('dr ' + fac.name.split()[-1])
        names.append(fac.name)
        names.append(fac.name.split()[-1])
        names.append(fac.name.split()[0])
        # offices
        if fac.office != None:
            buildings.add(fac.office.split('-')[0])
            rooms.add(fac.office.split('-')[1])
        # research fields
        if fac.research != None:
            for field in fac.research:
                fields.add(field)
        # position
        if fac.position != None:
            positions.add(fac.position.strip('.').strip().lower())
        if fac.facType != None:
            positions.add(fac.facType.strip('.').strip().lower())

    variables.append((NAME_VAR, names))
    variables.append((BUILDING_VAR, buildings))
    variables.append((ROOM_VAR, rooms))
    variables.append((FIELD_VAR, fields))
    variables.append((POS_VAR, positions))

    courseNames = set()
    for courses in getCoursesByTeacher().values():
        courseNames |= set([course.name.strip().lower() for course in courses])

    variables.append((COURSE_VAR, courseNames))

def getInfo():
    global facInfo
    if facInfo == None:
        refresh()
    return facInfo

def getCoursesByTeacher():
    global coursesByTeacher
    if coursesByTeacher == None:
        refresh()
    return coursesByTeacher

# finds variables within a query, returns the labeled query and the variables
def findVariables(query):
    info = getInfo()
    found = {}
    vard = query.lower().strip()
    vard = re.sub(' +', ' ', vard)
    # handle aliases
    words = []
    for word in vard.split():
        if word in akas:
            word = akas[word]
        words.append(word)
    vard = ' '.join(words)
    # find variables
    for varSet in variables:
        var = varSet[0]
        for val in varSet[1]:
            if len(val) <= len(regexTag) or val[:len(regexTag)] != regexTag:
                # exact matches
                ndx = vard.find(val)
                if ndx != -1:
                    found[ndx] = (var, val)
                    vard = vard[:ndx] + var + vard[ndx+len(val):]
            else:
                # handle regex
                regex = val[len(regexTag):]
                words = vard.split()
                ndx = 0
                for word in words:
                    reMatch = re.search(regex, word)
                    if reMatch != None:
                        # match
                        found[ndx] = (var, word)
                        vard = vard[:ndx] + var + vard[ndx+len(word):]
                    ndx += len(word) + 1
    # get order of variables
    queryVars = []
    for key in sorted(found):
        queryVars.append(found[key])
    return (vard, queryVars)



