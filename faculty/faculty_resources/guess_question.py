from sklearn.naive_bayes import BernoulliNB
from faculty_resources import faculty_rep_vars
from faculty_resources import faculty_update
from faculty_resources import answer_functions
from sklearn.linear_model import LinearRegression as LR
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import re
import numpy as np

QUESTION_TYPES = ['who', 'what', 'where', 'when', 'how', 'why']

KEYWORDS = ['office', 'phone', 'email', 'site', 'published', 'cited', 'number', 'contact', 'page', 'extension', 'building', 'room', 'senior project', 'research', 'teach', 'special', 'phd', 'focus', 'faculty', 'interest', 'locate', 'degree', 'doctorate', 'position', 'job']

TOPICS = {
    1:['study', 'interest', 'field', 'research', 'focus'], # research
    2:['office', 'buidling', 'room', 'contact', 'locate'], # location
    3:['phone', 'number', 'extension', 'contact', 'call'], # phone
    4:['degree', 'doctorate', 'phd', 'study', 'graduate'], # degree
    5:['hire', 'retire', 'work', 'position', 'job'] # faculty info
}

FULL_CORPUS = None # do not access directly

RATING_THRESHOLD = 0.02

def getCorpus():
    global FULL_CORPUS
    if FULL_CORPUS != None:
        return FULL_CORPUS
    # words from questions
    corpus = {}
    qs = getAnswers()
    stem = WordNetLemmatizer()
    for q in qs:
        for word in q.split():
            for c in ',.?!"' + "'":
                word = word.replace(c, '')
            w = stem.lemmatize(word)
            if '[' not in w and w not in stopwords.words('english'):
                if w not in corpus:
                    corpus[w] = 0
                corpus[w] += 1
    # weigh important words
    WEIGHT = 2
    for word_set in TOPICS.values():
        for w in word_set:
            if w not in corpus:
                corpus[w] = 0
            corpus[w] += WEIGHT
    FULL_CORPUS = corpus
    return FULL_CORPUS

def rate(query, query_vars):
    corpus = getCorpus()
    score = 0
    VAR_SCORE = 2 # how much using a faculty name affects score
    # score variables
    score = len(query_vars) * VAR_SCORE
    # check for corpus words
    stem = WordNetLemmatizer()
    for word in query.strip().split():
        stemmed = stem.lemmatize(word)
        if stemmed in corpus:
            score += corpus[stemmed]
    maxScore = sum(corpus.values())
    rating = score / maxScore
    return rating


def getTopic(s):
    scores = {}
    for topic in TOPICS:
        scores[topic] = 0
        for word in TOPICS[topic]:
            if word in s:
                scores[topic] += 1
    topic = sorted(scores, key=scores.get, reverse=True)[0]
    if scores[topic] == 0:
        topic = 0
    return topic

# gets the questions and ansers from questions.txt
def getAnswers():
    qs = {}
    with open('faculty_resources/questions.txt', 'r') as f:
        for line in f:
            ndx = line.find(' 1 |')
            if ndx == -1:
                ndx = line.find(' 0 |')
            parts = line.split('|')
            q = parts[0][2:-3].lower().strip()
            q = q.replace('?', '')
            a = parts[2].strip()
            qs[q] = a
    return qs

def getVarList():
    return [x[0][1:-1] for x in faculty_rep_vars.variables]

def getVarsInString(s):
    pattern = re.compile(r"\[([A-Za-z0-9_]+)\]")
    varList = []
    for match in re.findall(pattern, s):
      varList.append(match)
    return varList 

def buildFeatures(s):
    varList = getVarList()
    feats = []
    # has X variable
    foundVars = getVarsInString(s)
    for var in varList:
        feats.append(foundVars.count(var))
    # question type (who, what...)
    for var in QUESTION_TYPES:
        feats.append(var in s)
    # number of variables
    feats.append(len(foundVars))
    # topic
    feats.append(getTopic(s))
    return feats

def getModel(qs, query):
    getVarList()
    model = BernoulliNB()
    # get questions and answers (input and output)
    questions = list(qs.keys())
    answers = []
    # get answers, ignore functions if no variables
    foundVars = getVarsInString(query)
    for i in range(len(questions)):
        q = questions[i]
        if '[function]' not in qs[q] or len(foundVars) > 0:
            answers.append(qs[q])
        else:
            questions[i] = ''
    # remove bad questions
    temp = []
    for q in questions:
        if len(q) > 0:
            temp.append(q)
    questions = temp
    # add features
    for ndx in range(len(questions)):
        questions[ndx] = buildFeatures(questions[ndx])
    # convert for fitting
    X = np.array(questions)
    y = np.array(answers)
    # fit model
    model.fit(X, y)
    return model, answers

def guess(query, name):
    rating = 0
    query_str, query_vars = faculty_rep_vars.findVariables(query)
    qs = getAnswers()
    if query_str in qs:
        # exact match
        rating = 1
        return query_str, query_vars, qs[query_str], rating
    elif len(name) > 0 and "endnow" in query:
        return query_str, query_vars, "END:Done focusing on " + name, 1
    elif len(name) > 0:
        fac = answer_functions.getRelevantFaculty([name])[name]
        if query == 'factype':
            query = 'facType'
        if hasattr(fac, query):
            ans = getattr(fac, query)
            return query_str, query_vars, query + ' for ' + name + ' is ' + ans, 0.8
        else:
            return query_str, query_vars, 'Unknown field ' + query + ' for ' + name, 0.8
    else:
        # inexact match
        rating = rate(query_str, query_vars)
        # high enough rating
        model, answers = getModel(qs, query_str)
        feats = np.array(buildFeatures(query_str))
        feats = feats.reshape(1, -1)
        prediction = model.predict(feats)
        if rating <= RATING_THRESHOLD:
            # rating too low
            return query_str, query_vars, prediction, rating
        return query_str, query_vars, prediction[0], rating





