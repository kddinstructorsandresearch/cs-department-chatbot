import sys

OUR_KEY = '4'

if len(sys.argv) != 2:
    print('Usage: python3 csvToText.py [csv file name]')
    exit(-1)

fname = sys.argv[1]
if fname[-4:] != '.csv':
    print('File must be a .csv')
    exit(-1)
    
newFname = fname[:-3] + 'txt'
print('Converting "' + str(fname) + '" to "' + newFname + '"')
count = 0
with open(newFname, 'w+') as txt:
    with open(fname, 'r') as csv:
        lines = csv.readlines()
    for line in lines[1:]:
        count += 1
        parts = line.split(',')
        if len(parts) > 1:
            q = parts[0]
            partial = parts[1]
            url = parts[2]
            if len(url) > 0:
                url = ' ' + url
            ans = parts[3]
            if len(ans) > 0:
                ans = ' ' + ans
            txt.write(OUR_KEY + ' ' + q + ' ' + partial + ' |' + url + ' |' + ans + '\n')
print('Converted', count, 'questions.')
