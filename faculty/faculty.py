import nltk, random
from faculty_resources import faculty_rep_vars
from faculty_resources import guess_question as guess
from faculty_resources import faculty_update
from faculty_resources import answer_functions

FUNC_TAG = '[function]'
THRESH = 0.02

class facultyModule:
    moduleContributors = ['Gavin Scott','Jose Roman','Andrew Reinman', 'Andrew Vorhees', 'Andy DuFrene']
    moduleName = "Faculty"
    moduleDescription = "Some description"    
    numberOfResponses = 0

    def __init__(self):  
        self.dataStore = [] # you may read data from a pickeled file or other sources
        self.person = ''

    def id(self):
        return [self.moduleName,self.moduleDescription]

    def credits(self):
        return ', '.join(self.moduleContributors)

    def update(self):
        #some update procedure dealing with dataStore, if items need to be crawled and saved, this is the place to do that
        return faculty_update.update()

    def getRating(self, query, history = []):    #get rating based on query and (if you wish) the history
        query_str, query_vars, answer, rating = guess.guess(query)
        return rating

    def response(self, query, history = []):
        query = query.lower()
        if len(query) > 0:  #signals can be "Normal", "Error", "Question", "Unknown" or "End"
            signal = "Normal"
        else:
            signal = "Error"
        query_str, query_vars, answer, rating = guess.guess(query, self.person)
        if rating < THRESH:
            response_string = 'No answer for that question!'
        else:
            response_string = answer
            response_string = answer
            if response_string[:len(FUNC_TAG)] == FUNC_TAG:
                funcName = response_string.split(FUNC_TAG)[1].strip().replace('()','')
                func = getattr(answer_functions, funcName)
                try:
                    response_string = func(query_str, query_vars)
                except:
                    response_string = "I don't know!"
                    rating = 0
                if rating > 0 and "FOLLOWUP" in response_string and response_string.split("UP:")[0] == "FOLLOW":
                    self.person = response_string.split("UP:")[1]
                    fac = answer_functions.getRelevantFaculty([self.person])[self.person]
                    attrs = dir(fac)
                    attrs2 = []
                    for attr in attrs:
                        if '__' not in attr and attr != 'pr' and attr != 'toString' and getattr(fac, attr):
                            attrs2.append(attr)
                    return ([1,'Question', 'What do you want to know about ' + self.person.title() \
                                              + '? \n\tI know about their ' + ", ".join(attrs2) + '\n\tChoose a field or type ENDNOW to stop talking about ' + self.person.title()])
            elif type(response_string) == str and response_string.split(":")[0] == "END":
                self.person = ""
                return ([1,'End', response_string.split(":")[1]])
        return ([rating, signal, response_string])



def test():
    facMod = facultyModule()
    response = ''
    history = []
    query_str = ''
    while True:
        print("How can I help you? (\"quit\" to exit)",end= " ")
        query_raw = input()
        if query_raw.lower() == 'update':
            facMod.update()
        elif query_raw.lower() in ['quit', 'exit']:
            exit()
        else:
            queries = query_raw.split('?')
            for query_str in [x for x in queries if len(x) > 0]:
                response = facMod.response(query_str, history)
                history.append([query_str, response])
                if response[1] is "Error":
                    print('\t', response[1])
                else:
                    print('\t', response[2])
    
if __name__ == "__main__":
    test()    
