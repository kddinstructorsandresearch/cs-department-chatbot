import sys
import re

if len(sys.argv) != 3:
    print('Usage: find_duplicates.py [text file 1] [text file 2]')

f1 = sys.argv[1]
f2 = sys.argv[2]

print('Looking for duplicates in', f1, 'and', f2)

def removePunctuation(s):
    for p in ".?', ":
        s = s.replace(p, '')
    return s

def getQuestions(f):
    qs = {}
    for line in open(f, 'r'):
        q = line.strip().lower()[2:]
        ndx = q.find(' 1 |')
        if ndx == -1:
            ndx = q.find(' 0 |')
        q = removePunctuation(q[:ndx])
        q =  re.sub('\[(.*?)\]', '[]', q)
        qs[q] = line.strip()
    return qs


q1 = getQuestions(f1)
q2 = getQuestions(f2)

duplicates = []
for q in q1:
    if q in q2:
        duplicates.append(q)

print('\n\nFOUND', len(duplicates), 'DUPLICATES\n\n')
for d in duplicates:
    print(q1[d])
    print(q2[d])
    print()

print('**********************************************')
if len(q1) != len(set(q1)):
    print("Also found duplicates within", f1)
if len(q2) != len(set(q2)):
    print("Also found duplicates within", f2)

